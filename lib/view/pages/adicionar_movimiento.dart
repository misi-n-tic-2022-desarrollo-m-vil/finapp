import 'package:flutter/material.dart';
import '/services/database_handler.dart';
import '/model/objetos.dart';
import 'widgets/drawer.dart';

class PaginaGastos extends StatefulWidget {
  const PaginaGastos({Key? key}) : super(key: key);

  @override
  _Gastos createState() => _Gastos();
}

class _Gastos extends State<PaginaGastos> {
  final GlobalKey<FormState> _formKey = GlobalKey();
  late DatabaseHandler handler;
  // - - - CREAMOS EL CONTROLADOR DEL CAMPO DE TEXTO DEL GASTO Y SU VALOR - - -
  final NombreGasto = TextEditingController();
  final ValorGasto = TextEditingController();
  String Gasto = "";
  String Valor = "";
  // - - - FIN CREACION CONTROLADOR DE CAMPO DE NOMBRE - - - -

  //- - - CREAMOS LAS LISTAS DE CATEGORÍAS Y MOVIMIENTOS QUE SE MOSTRARAN EN LA LISTA DESPLEGABLE
  List<String> items = [
    "Gasolina",
    "Agua",
    "Mercado",
    "Mantenimiento vehículo"
  ];
  String TipoGasto = "";

  List<String> items2 = ["Ahorro", "Gasto", "Ingreso"];
  String TipoMovimiento = "";
  // - - - FIN CREACION LISTAS - - -

  @override
  void initState() {
    TipoGasto = items[0]; //PARA LA LISTA DESPLEGABLE GASTOS
    TipoMovimiento = items2[0]; //PARA LA LISTA DESPLEGABLE MOVIMIENTOS
    super.initState();
    this.handler = DatabaseHandler();
  }

  Future<int> addGasto(Gasto, costo, TipoGasto, TipoMovimiento) async {
    Objeto gastos = Objeto(
        name: Gasto,
        valor: costo,
        tipogasto: TipoGasto,
        movimiento: TipoMovimiento);
    //User secondUser = User(name: "john", age: 31, country: "United Kingdom");
    List<Objeto> listaDeGastos = [gastos];

    return await this.handler.insertGasto(listaDeGastos);
  }

  //FUNCION PARA REGISTRAR DATOS ENVIADOS DESDE EL FORMULARIO A LA BD
  void _saveForm(NombreGasto, ValorGasto, TipoGasto, TipoMovimiento) {
    final bool isValid = _formKey.currentState!.validate();
    if (isValid) {
      /**
      if (kDebugMode) {
        //Funcion proporcionada por foundation.dart
        print('Got a valid input');
      }
      */

      String costo = ValorGasto.text.toString();
      Gasto = NombreGasto.text;
      this.handler = DatabaseHandler();
      this.handler.initializeDB().whenComplete(() async {
        await this.addGasto(Gasto, costo, TipoGasto, TipoMovimiento);
        setState(() {});
      });
      print('Got a valid input');
      // And do something here
    }
  }

  @override
  void dispose() {
    // Limpia el controlador cuando el Widget se descarte
    NombreGasto.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                padding: const EdgeInsets.all(8.0),
                child: const Text('Nuevo Registro'),
              ),
              const SizedBox(
                width: 100,
              ),
              Image.asset(
                "assets/images/logo.png",
                fit: BoxFit.contain,
                height: 60,
              ),
            ],
          ),
          flexibleSpace: Container(
            decoration: const BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: <Color>[Color(0xFF4677C6), Color(0xFF1F3A66)])),
          )),
      drawer: const DrawerWidget(),
      body: SingleChildScrollView(
        child: Center(
          child: Column(
            //mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Container(
                margin: EdgeInsets.only(
                  top: 5,
                ),
                child: Form(
                  key: _formKey,
                  child: Column(
                    children: [
                      TextFormField(
                        controller: NombreGasto,
                        validator: (value) {
                          if (value != null && value.trim().length < 3) {
                            return 'Este campo requiere al emnos 3 caracteres';
                          }
                          return null;
                        },
                        decoration: const InputDecoration(
                            labelText: 'Ingrese el nombre del movimiento',
                            border: OutlineInputBorder(),
                            errorBorder: OutlineInputBorder(
                                borderSide:
                                    BorderSide(color: Colors.red, width: 5))),
                      ),
                      TextFormField(
                          controller: ValorGasto,
                          keyboardType: TextInputType.number,
                          validator: (value) {
                            if (value != null && value.trim().length < 3) {
                              return 'Este campo requiere al emnos 3 caracteres';
                            }
                            return null;
                          },
                          decoration: const InputDecoration(
                              labelText: "Ingrese el valor del movimiento",
                              border: OutlineInputBorder(),
                              errorBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Colors.red, width: 5)))),
                      const SizedBox(
                        height: 10,
                      ),
                    ],
                  ),
                ),
              ),

              //PONEMOS UNA ETIQUETA A LA LISTA DESPLEGABLE
              Container(
                child: Text(
                  "Seleccione una Categoría",
                  style: TextStyle(height: 2, fontSize: 22),
                ),
              ),
              //CREAMOS LA LISTA DESPLEGABLE DE CATEGORÍA DE GASTOS
              Container(
                height: 40,
                width: 400,
                margin: EdgeInsets.only(
                  top: 1,
                  left: 20,
                ),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: Colors.blueGrey[50],
                ),
                child: DropdownButton(
                  //alignment: Alignment.topCenter,
                  borderRadius: BorderRadius.circular(8),
                  //dropdownColor: Colors.blueAccent,
                  value: TipoGasto,
                  items: items
                      .map<DropdownMenuItem<String>>(
                        (e) => DropdownMenuItem(
                          value: e,
                          child: Text(e),
                          alignment: Alignment.center,
                        ),
                      )
                      .toList(),
                  onChanged: (String? value) => setState(
                    () {
                      if (value != null) TipoGasto = value;
                    },
                  ),
                ),
              ),
              //MUESTRA TEXTO SELECCIONADO DE LA LISTA
              Container(
                margin: EdgeInsets.only(
                  top: 5,
                ),
                child: Text(TipoGasto),
              ),
              //CREAMOS LA LISTA DESPLEGABLE DE TIPO DE MOVIMIENTO
              //PONEMOS UNA ETIQUETA A LA LISTA DESPLEGABLE
              Container(
                child: Text(
                  "Tipo de movimiento",
                  style: TextStyle(height: 2, fontSize: 22),
                ),
              ),
              Container(
                height: 40,
                width: 400,
                margin: EdgeInsets.only(
                  top: 1,
                  left: 20,
                ),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: Colors.blueGrey[50],
                ),
                child: DropdownButton(
                  //alignment: Alignment.topCenter,
                  borderRadius: BorderRadius.circular(8),
                  //dropdownColor: Colors.blueAccent,
                  value: TipoMovimiento,
                  items: items2
                      .map<DropdownMenuItem<String>>(
                        (d) => DropdownMenuItem(
                          value: d,
                          child: Text(d),
                          alignment: Alignment.center,
                        ),
                      )
                      .toList(),
                  onChanged: (String? value2) => setState(
                    () {
                      if (value2 != null) TipoMovimiento = value2;
                    },
                  ),
                ),
              ),
              //MUESTRA TEXTO SELECCIONADO DE LA LISTA
              Container(
                margin: EdgeInsets.only(
                  top: 5,
                ),
                child: Text(TipoMovimiento),
              ),
              Container(
                  width: 400,
                  height: 40,
                  child: ElevatedButton.icon(
                      style: ElevatedButton.styleFrom(
                        backgroundColor: const Color.fromARGB(255, 19, 77, 142),
                      ),
                      onPressed: () {
                        _saveForm(
                            NombreGasto, ValorGasto, TipoGasto, TipoMovimiento);
                        final nav = Navigator.of(context);
                        nav.pop();
                      },
                      icon: const Icon(Icons.save),
                      label: const Text('Registrar'))),
            ],
          ),
        ),
      ),
    );
  }
}

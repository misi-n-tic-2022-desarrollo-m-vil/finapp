import "package:flutter/material.dart";

class CheckboxFormField extends StatefulWidget {
  const CheckboxFormField({super.key});

  @override
  State<CheckboxFormField> createState() => _CheckboxFormFieldState();
}

class _CheckboxFormFieldState extends State<CheckboxFormField> {
  bool checkboxValue = false;

  @override
  Widget build(BuildContext context) {
    return FormField(
      builder: (state) {
        return Column(children: [
          Row(children: [
            Checkbox(
                value: checkboxValue,
                onChanged: (value) {
                  checkboxValue = value!;
                  state.didChange(value);
                }),
            const Text(" Acepto",
                style: TextStyle(
                    color: Colors.blueGrey, fontWeight: FontWeight.bold)),
            InkWell(
              onTap: () {},
              child: const Text(
                " términos",
                style:
                    TextStyle(color: Colors.blue, fontWeight: FontWeight.bold),
              ),
            ),
            const Text(" & ",
                style: TextStyle(
                    color: Colors.blueGrey, fontWeight: FontWeight.bold)),
            InkWell(
              onTap: () {},
              child: const Text(
                "condiciones de servicio",
                style:
                    TextStyle(color: Colors.blue, fontWeight: FontWeight.bold),
              ),
            ),
          ]),
          Text(
            state.errorText ?? "",
            style: TextStyle(
              color: Theme.of(context).errorColor,
              fontSize: 12,
            ),
          )
        ]);
      },
      validator: (value) {
        if (!checkboxValue) {
          return "Debe aceptar los términos y condiciones de servicio";
        }
        return null;
      },
    );
  }
}

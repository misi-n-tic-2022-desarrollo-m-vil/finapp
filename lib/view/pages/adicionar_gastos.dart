import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import '../../model/objetos.dart';
import '../../services/database_handler.dart';
import 'widgets/drawer.dart';

class PaginaRegistroPpto extends StatefulWidget {
  const PaginaRegistroPpto(
      {Key? key, required String email, required String name})
      : super(key: key);

  @override
  _Gastos createState() => _Gastos();
}

class _Gastos extends State<PaginaRegistroPpto> {
  final GlobalKey<FormState> _formKey = GlobalKey();
  late DatabaseHandlerPresupuesto handler;
  final NombreGasto = TextEditingController();
  final ValorGasto = TextEditingController();
  String Gasto = "";
  String Valor = "";
  List<String> items = ["Ingreso", "Ahorro", "Gasto"];
  String TipoGasto = "";

  List<String> items2 = [
    'Mercado',
    'Servicio Electrico',
    'Servicio de Gas',
    'Servicio de agua',
    'Telefono-Internet-Televisión',
    'Telefonia Movil',
    'Transporte',
    'Sueldo',
    'Ingreso'
  ];
  String TipoMovimiento = "";

  @override
  void initState() {
    TipoGasto = items[0];
    TipoMovimiento = items2[0];
    super.initState();
    handler = DatabaseHandlerPresupuesto();
  }

  Future<int> addGasto(Gasto, costo, TipoGasto, TipoMovimiento) async {
    Objeto gastos = Objeto(
        name: Gasto,
        valor: costo,
        tipogasto: TipoGasto,
        movimiento: TipoMovimiento);
    List<Objeto> listaDeGastos = [gastos];

    return await handler.insertGasto(listaDeGastos);
  }

  void _saveForm(NombreGasto, ValorGasto, TipoGasto, TipoMovimiento) {
    final bool isValid = _formKey.currentState!.validate();
    if (isValid) {
      String costo = ValorGasto.text.toString();
      Gasto = NombreGasto.text;
      handler = DatabaseHandlerPresupuesto();
      handler.initializeDB().whenComplete(() async {
        await addGasto(Gasto, costo, TipoGasto, TipoMovimiento);
        setState(() {});
      });
      if (kDebugMode) {
        print('Got a valid input');
      }
    }
  }

  @override
  void dispose() {
    NombreGasto.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                padding: const EdgeInsets.all(8.0),
                child: const Text('Nuevo Registro'),
              ),
              const SizedBox(
                width: 100,
              ),
              Image.asset(
                "assets/images/logo.png",
                fit: BoxFit.contain,
                height: 60,
              ),
            ],
          ),
          flexibleSpace: Container(
            decoration: const BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: <Color>[Color(0xFF4677C6), Color(0xFF1F3A66)])),
          )),
      drawer: const DrawerWidget(),
      body: SingleChildScrollView(
        child: Column(
          children: [
            const Text(
              " ",
              style: TextStyle(height: 2, fontSize: 18),
            ),
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 15),
              margin: const EdgeInsets.symmetric(horizontal: 15),
              child: Form(
                key: _formKey,
                child: Column(
                  children: [
                    TextFormField(
                        controller: NombreGasto,
                        keyboardType: TextInputType.number,
                        validator: (value) {
                          if (value != null && value.trim().isEmpty) {
                            return 'Este campo requiere al menos 1 caracter';
                          }
                          return null;
                        },
                        decoration: const InputDecoration(
                            labelText: "Ingrese el valor del presupuesto",
                            border: OutlineInputBorder(),
                            errorBorder: OutlineInputBorder(
                                borderSide:
                                    BorderSide(color: Colors.red, width: 5)))),
                    const SizedBox(
                      height: 20,
                    ),
                  ],
                ),
              ),
            ),
            const Text(
              "Seleccione una Clase",
              style: TextStyle(height: 2, fontSize: 18),
            ),
            Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  border: Border.all(color: Colors.black)),
              padding: const EdgeInsets.symmetric(horizontal: 20),
              margin: const EdgeInsets.symmetric(horizontal: 20),
              child: DropdownButton(
                borderRadius: BorderRadius.circular(8),
                value: TipoGasto,
                items: items
                    .map<DropdownMenuItem<String>>(
                      (e) => DropdownMenuItem(
                        value: e,
                        alignment: Alignment.center,
                        child: Text(e),
                      ),
                    )
                    .toList(),
                onChanged: (String? value) => setState(
                  () {
                    if (value != null) TipoGasto = value;
                  },
                ),
              ),
            ),
            const Text(
              "Seleccione una Categoria",
              style: TextStyle(height: 2, fontSize: 18),
            ),
            Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  border: Border.all(color: Colors.black)),
              padding: const EdgeInsets.symmetric(horizontal: 20),
              margin: const EdgeInsets.symmetric(horizontal: 20),
              child: DropdownButton(
                borderRadius: BorderRadius.circular(8),
                value: TipoMovimiento,
                items: items2
                    .map<DropdownMenuItem<String>>(
                      (d) => DropdownMenuItem(
                        value: d,
                        alignment: Alignment.center,
                        child: Text(d),
                      ),
                    )
                    .toList(),
                onChanged: (String? value2) => setState(
                  () {
                    if (value2 != null) TipoMovimiento = value2;
                  },
                ),
              ),
            ),
            Container(
              margin: const EdgeInsets.all(15),
              width: MediaQuery.of(context).size.width - 30,
              child: ElevatedButton.icon(
                style: ElevatedButton.styleFrom(
                  backgroundColor: Color.fromARGB(255, 19, 77, 142),
                ),
                onPressed: () {
                  _saveForm(NombreGasto, ValorGasto, TipoGasto, TipoMovimiento);
                  final nav = Navigator.of(context);
                  nav.pop();
                },
                icon: const Icon(Icons.save),
                label: const Text("Registrar"),
              ),
            )
          ],
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'widgets/drawer.dart';

class PaginaInformes extends StatelessWidget {
  const PaginaInformes({super.key});

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        return false;
      },
      child: Scaffold(
        appBar: AppBar(
            title: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Container(
                  padding: const EdgeInsets.all(8.0),
                  child: const Text('Informes'),
                ),
                const SizedBox(
                  width: 100,
                ),
                Image.asset(
                  "assets/images/logo.png",
                  fit: BoxFit.contain,
                  height: 60,
                ),
              ],
            ),
            flexibleSpace: Container(
              decoration: const BoxDecoration(
                  gradient: LinearGradient(
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      colors: <Color>[Color(0xFF4677C6), Color(0xFF1F3A66)])),
            )),
        drawer: const DrawerWidget(),
        body: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Image.asset("assets/images/construction.png"),
          ],
        ),
      ),
    );
  }
}

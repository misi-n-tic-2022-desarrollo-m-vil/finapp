import 'package:finapp_iniciosesion/view/pages/adicionar_gastos.dart';
import 'package:flutter/material.dart'
    show
        Alignment,
        AppBar,
        AsyncSnapshot,
        BoxDecoration,
        BoxFit,
        BuildContext,
        Card,
        Center,
        CircularProgressIndicator,
        Color,
        Colors,
        Container,
        DismissDirection,
        Dismissible,
        EdgeInsets,
        FloatingActionButton,
        FutureBuilder,
        Icon,
        Icons,
        Image,
        Key,
        LinearGradient,
        ListTile,
        ListView,
        MainAxisAlignment,
        MaterialPageRoute,
        Navigator,
        Row,
        Scaffold,
        SizedBox,
        State,
        StatefulWidget,
        Text,
        ValueKey,
        Widget,
        WillPopScope;
import '../../model/objetos.dart';
import '../../services/database_handler.dart';
import 'widgets/drawer.dart';

class PaginaPpto extends StatefulWidget {
  const PaginaPpto({
    Key? key,
    required String email,
    required String name,
  }) : super(key: key);

  @override
  _VistaG createState() => _VistaG();
}

class _VistaG extends State<PaginaPpto> {
  late DatabaseHandlerPresupuesto handler;

  @override
  void initState() {
    super.initState();
    handler = DatabaseHandlerPresupuesto();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        return false;
      },
      child: Scaffold(
        appBar: AppBar(
            title: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Container(
                  padding: const EdgeInsets.all(8.0),
                  child: const Text('Presupuesto'),
                ),
                const SizedBox(
                  width: 100,
                ),
                Image.asset(
                  "assets/images/logo.png",
                  fit: BoxFit.contain,
                  height: 60,
                ),
              ],
            ),
            flexibleSpace: Container(
              decoration: const BoxDecoration(
                  gradient: LinearGradient(
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      colors: <Color>[Color(0xFF4677C6), Color(0xFF1F3A66)])),
            )),
        drawer: const DrawerWidget(),
        body: FutureBuilder(
          future: handler.obtenerMovimientos(),
          builder:
              (BuildContext context, AsyncSnapshot<List<Objeto>> snapshot) {
            if (snapshot.hasData) {
              return ListView.builder(
                itemCount: snapshot.data?.length,
                itemBuilder: (BuildContext context, int index) {
                  return Dismissible(
                    direction: DismissDirection.endToStart,
                    background: Container(
                      color: Colors.red,
                      alignment: Alignment.centerRight,
                      padding: const EdgeInsets.symmetric(horizontal: 10.0),
                      child: const Icon(Icons.delete_forever),
                    ),
                    key: ValueKey<int>(snapshot.data![index].id!),
                    onDismissed: (DismissDirection direction) async {
                      await handler.borrarMovimiento(snapshot.data![index].id!);
                      setState(() {
                        snapshot.data!.remove(snapshot.data![index]);
                      });
                    },
                    child: Card(
                        child: ListTile(
                      contentPadding: const EdgeInsets.all(8.0),
                      leading: Text(snapshot.data![index].tipogasto.toString()),
                      title: Text(snapshot.data![index].name),
                      subtitle: Text(snapshot.data![index].valor.toString()),
                      trailing:
                          Text(snapshot.data![index].movimiento.toString()),
                    )),
                  );
                },
              );
            } else {
              return const Center(child: CircularProgressIndicator());
            }
          },
        ),
        floatingActionButton: FloatingActionButton(
          backgroundColor: Colors.yellow,
          foregroundColor: const Color.fromARGB(255, 1, 46, 83),
          onPressed: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => const PaginaRegistroPpto(
                          email: '',
                          name: '',
                        )));
          },
          child: const Icon(Icons.add),
        ),
      ),
    );
  }
}

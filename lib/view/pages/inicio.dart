import 'package:finapp_iniciosesion/view/pages/widgets/drawer.dart';
import 'package:flutter/material.dart';

import 'pagina_movimientos.dart';

class Inicio extends StatelessWidget {
  const Inicio({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                padding: const EdgeInsets.all(8.0),
                child: const Text('Inicio'),
              ),
              const SizedBox(
                width: 180,
              ),
              Image.asset(
                "assets/images/logo.png",
                fit: BoxFit.contain,
                height: 60,
              ),
            ],
          ),
          flexibleSpace: Container(
            decoration: const BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: <Color>[Color(0xFF4677C6), Color(0xFF1F3A66)])),
          )),
      drawer: const DrawerWidget(),
      body: SingleChildScrollView(
        padding: const EdgeInsets.all(8.0),
        child: WillPopScope(
          onWillPop: () async {
            return false;
          },
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                margin: const EdgeInsets.all(15),
                width: MediaQuery.of(context).size.width - 30,
                child: ElevatedButton(
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => const VistaGastos()));
                  },
                  child: const Text("Presupuesto"),
                ),
              ),
              const SizedBox(
                width: 30,
              ),
              Column(
                children: const <Text>[
                  Text("Registro"),
                ],
              ),
              const SizedBox(
                width: 30,
              ),
              Column(
                children: const <Text>[
                  Text("Informes"),
                ],
              ),
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.yellow,
        foregroundColor: const Color.fromARGB(255, 1, 38, 68),
        onPressed: () {},
        child: const Icon(Icons.add),
      ),
    );
  }
}

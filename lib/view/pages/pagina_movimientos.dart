import 'package:flutter/material.dart';
import '/services/database_handler.dart';
import '/model/objetos.dart';
import 'adicionar_movimiento.dart';
import 'widgets/drawer.dart';

class VistaGastos extends StatefulWidget {
  const VistaGastos({Key? key}) : super(key: key);

  @override
  _VistaG createState() => _VistaG();
}

class _VistaG extends State<VistaGastos> {
  late DatabaseHandler handler;

  @override
  void initState() {
    super.initState();
    this.handler = DatabaseHandler();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        return false;
      },
      child: Scaffold(
        appBar: AppBar(
            title: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Container(
                  padding: const EdgeInsets.all(8.0),
                  child: const Text('Movimientos'),
                ),
                const SizedBox(
                  width: 100,
                ),
                Image.asset(
                  "assets/images/logo.png",
                  fit: BoxFit.contain,
                  height: 60,
                ),
              ],
            ),
            flexibleSpace: Container(
              decoration: const BoxDecoration(
                  gradient: LinearGradient(
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      colors: <Color>[Color(0xFF4677C6), Color(0xFF1F3A66)])),
            )),
        drawer: const DrawerWidget(),
        body: FutureBuilder(
          future: this.handler.obtenerMovimientos(),
          builder:
              (BuildContext context, AsyncSnapshot<List<Objeto>> snapshot) {
            if (snapshot.hasData) {
              return ListView.builder(
                itemCount: snapshot.data?.length,
                itemBuilder: (BuildContext context, int index) {
                  return Dismissible(
                    direction: DismissDirection.endToStart,
                    background: Container(
                      color: Colors.red,
                      alignment: Alignment.centerRight,
                      padding: EdgeInsets.symmetric(horizontal: 10.0),
                      child: Icon(Icons.delete_forever),
                    ),
                    key: ValueKey<int>(snapshot.data![index].id!),
                    onDismissed: (DismissDirection direction) async {
                      await this
                          .handler
                          .borrarMovimiento(snapshot.data![index].id!);
                      setState(() {
                        snapshot.data!.remove(snapshot.data![index]);
                      });
                    },
                    child: Card(
                        child: ListTile(
                      contentPadding: EdgeInsets.all(8.0),
                      leading: Text(snapshot.data![index].tipogasto.toString()),
                      title: Text(snapshot.data![index].name),
                      subtitle: Text(snapshot.data![index].valor.toString()),
                      trailing:
                          Text(snapshot.data![index].movimiento.toString()),
                    )),
                  );
                },
              );
            } else {
              return Center(child: CircularProgressIndicator());
            }
          },
        ),
        floatingActionButton: FloatingActionButton(
          backgroundColor: Colors.yellow,
          foregroundColor: const Color.fromARGB(255, 1, 46, 83),
          onPressed: () {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => const PaginaGastos()));
            /**
            .then((data) {
            if (data != null) {
              getAllUserDetails();
              _showSuccessSnackBar('User Detail Added Success');
            }
          });
          */
          },
          child: const Icon(Icons.add),
        ),
      ),
    );
  }
}

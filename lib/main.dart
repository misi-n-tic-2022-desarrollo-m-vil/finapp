import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'firebase_options.dart';
import 'view/pages/iniciosesion.dart';
import 'view/pages/pagina_movimientos.dart';

void main(List<String> args) async {
  WidgetsFlutterBinding.ensureInitialized();

  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );

  FlutterError.onError = (details) {
    FirebaseCrashlytics.instance.recordFlutterFatalError(details);
  };

  PlatformDispatcher.instance.onError = (exception, stackTrace) {
    FirebaseCrashlytics.instance
        .recordError(exception, stackTrace, fatal: true);
    return true;
  };

  runApp(const FinApp());
}

class FinApp extends StatefulWidget {
  const FinApp({super.key});

  @override
  State<FinApp> createState() => _FinAppState();
}

class _FinAppState extends State<FinApp> {
  final _pref = SharedPreferences.getInstance();
  Widget _init = const Scaffold(
    body: Center(
      child: CircularProgressIndicator(),
    ),
  );

  @override
  void initState() {
    super.initState();
    _pref.then((pref) {
      setState(() {
        if (pref.getString("uid") != null) {
          _init = const VistaGastos();
        } else {
          _init = InicioSesion();
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "FinApp",
      home: _init,
    );
  }
}

import 'package:finapp_iniciosesion/model/entity/user.dart';
import 'package:finapp_iniciosesion/model/repository/fb_storage.dart';
import '../model/repository/fb_auth.dart';
import '../model/repository/user.dart';
import 'request/login.dart';
import 'request/registrousuario.dart';
import 'response/userinfo.dart';

class LoginController {
  late final UserRepository _userRepository;
  late final FirebaseAuthenticationRepository _authRepository;
  late FirebaseStorageRepository _storageRepository;

  LoginController() {
    _userRepository = UserRepository();
    _authRepository = FirebaseAuthenticationRepository();
    _storageRepository = FirebaseStorageRepository();
  }

  Future<UserInfoResponse> validateEmailPassword(LoginRequest request) async {
    await _authRepository.signInEmailPassword(request.email, request.password);
    var user = await _userRepository.findByEmail(request.email);
    return UserInfoResponse(
      id: user.id,
      photo: user.photo,
      name: user.name,
      email: user.email,
      isAdmin: user.isAdmin,
    );
  }

  Future<void> registerNewUser(RegistroUsuarioRequest request,
      {bool adminUser = false}) async {
    try {
      await _userRepository.findByEmail(request.email);
      return Future.error("Ya existe un usuario con este correo electrónico");
    } catch (e) {
      await _authRepository.createEmailPasswordAccount(
          request.email, request.password);

      _userRepository.save(UserEntity(
          photo: request.photo,
          name: request.name,
          email: request.email,
          isAdmin: adminUser));
    }
  }

  Future<void> save(UserEntity user) async {
    if (user.photo != null) {
      var url = await _storageRepository.loadFile(user.photo!, "sale/photo");
      user.photo = url;
    }

    await _userRepository.save(user);
  }

  Future<void> logoutUser() async {
    await _authRepository.signOutEmailPassword();
  }

  Future<String> updatePhoto(String uid, String photo) async {
    photo = await _storageRepository.loadFile(photo, "user/photo");
    await _userRepository.updatePhoto(uid, photo);
    return photo;
  }
}

class UserInfoResponse {
  late String? id;
  late String? photo;
  late String? name;
  late String? email;
  late bool? isAdmin;

  UserInfoResponse({this.id, this.photo, this.name, this.email, this.isAdmin});
}

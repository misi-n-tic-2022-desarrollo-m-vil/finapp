class RegistroUsuarioRequest {
  late String photo;
  late String name;
  late String email;
  late String password;

  @override
  String toString() {
    return "$photo, $name, $email, $password";
  }
}

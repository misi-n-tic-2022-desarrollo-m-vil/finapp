class Objeto {
  final int? id;
  final String name;
  final String valor;
  final String tipogasto;
  final String? movimiento;

  Objeto(
      {this.id,
      required this.name,
      required this.valor,
      required this.tipogasto,
      this.movimiento});

  Objeto.fromMap(Map<String, dynamic> res)
      : id = res["id"],
        name = res["name"],
        valor = res["valor"],
        tipogasto = res["tipogasto"],
        movimiento = res["movimiento"];

  Map<String, Object?> toMap() {
    return {
      'id': id,
      'name': name,
      'valor': valor,
      'tipogasto': tipogasto,
      'movimiento': movimiento
    };
  }
}

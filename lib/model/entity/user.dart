import 'package:cloud_firestore/cloud_firestore.dart';

class UserEntity {
  late String? id;
  late String? photo;
  late String? name;
  late String? email;
  late bool? isAdmin;

  UserEntity({this.id, this.photo, this.name, this.email, this.isAdmin});

  factory UserEntity.fromFirestore(
      DocumentSnapshot<Map<String, dynamic>> snapshot,
      SnapshotOptions? options) {
    var data = snapshot.data();

    return UserEntity(
        id: snapshot.id,
        photo: data?["photo"],
        name: data?["name"],
        email: data?["email"],
        isAdmin: data?["isAdmin"]);
  }

  Map<String, dynamic> toFirestore() {
    return {
      if (photo != null && photo!.isNotEmpty) "photo": photo,
      if (name != null && name!.isNotEmpty) "name": name,
      if (email != null && name!.isNotEmpty) "email": email,
      //"isAdmin" : isAdmin == null ? false : isAdmin : isAdmin;
      "isAdmin": isAdmin ?? false
    };
  }

  @override
  String toString() {
    return "UserEntity {$photo, $name, $email, $isAdmin}";
  }
}
